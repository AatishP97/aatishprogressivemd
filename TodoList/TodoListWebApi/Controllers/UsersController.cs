﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TodoListEntityFramework;

namespace TodoListWebApi.Controllers {

    public class UsersController : ApiController {
        private TODOLISTEntities entities = new TODOLISTEntities();

        //api/users
        //GET
        public List<User> Get() {
            return entities.Users.ToList();
        }

        //api/users
        //POST
        public IHttpActionResult Post([FromBody] User user) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            entities.Users.Add(user);
            entities.SaveChanges();

            return Ok();
        }

        //api/users
        //DELETE 
        public IHttpActionResult Delete(String username) {
            User user = entities.Users.Find(username);
            if (user == null) {
                return NotFound();  //If the user isn't in the database, return 404 Not Found
            }

            entities.Users.Remove(user);
            entities.SaveChanges();
            return Ok();
        }

        //api/users
        //PUT
        public IHttpActionResult Put(String username, [FromBody]User user) {
            User changed_user = entities.Users.Find(username);
            if (user == null) {
                return NotFound();
            }

            changed_user.Password = user.Password;
            changed_user.Email = user.Email;
            entities.SaveChanges();

            return Ok();
        }
    }
}
