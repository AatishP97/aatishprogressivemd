﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TodoListEntityFramework;

namespace TodoListWebApi.Controllers {
    public class TasksController : ApiController {
        private TODOLISTEntities entities = new TODOLISTEntities();

        //api/tasks
        //GET
        public List<Task> GetTasks() {
            return entities.Tasks.ToList();
        }

        //api/tasks/Aatish
        //GET
        [ResponseType(typeof(List<Task>))]
        public IHttpActionResult GetTask(String username) {
            var categoryList = entities.Categories.Where(c => c.BelongsTo == username).ToList<Category>();   //Get categories for username
            if (categoryList == null) {
                return NotFound();
            }
            List<Task> taskList = new List<Task>();
            foreach (var cat in categoryList) {
                var tasks = entities.Tasks.Where(t => t.Category == cat.CategoryID);    //Fetch all tasks in category
                foreach (var task in tasks) {
                    taskList.Add(task); //Add each task to the list to be returned
                }
            }

            if (taskList == null) {
                return NotFound();    //Return empty list since no tasks are found
            }

            return Ok(taskList);
        }

        //api/tasks
        //POST
        public IHttpActionResult Post(Task task) {
            ModelState.Remove("CreatedAt"); //Don't check these because they weren't passed in to the model
            ModelState.Remove("UpdatedAt");
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            var now = DateTime.Now;
            task.CreatedAt = now;
            task.UpdatedAt = now;

            entities.Tasks.Add(task);
            entities.SaveChanges();

            return Ok();
        }

        //api/tasks
        //DELETE
        public IHttpActionResult Delete(int taskID) {
            Task task = entities.Tasks.Find(taskID);
            if (task == null) {
                return NotFound();
            }

            entities.Tasks.Remove(task);
            entities.SaveChanges();

            return Ok();
        }

        //api/tasks
        //PUT
        public IHttpActionResult Put(int taskID, [FromBody]Task task) {
            Task changed_task = entities.Tasks.Find(taskID);
            if (task == null) {
                return NotFound();
            }

            ModelState.Remove("CreatedAt");
            ModelState.Remove("UpdatedAt");
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            changed_task.Title = task.Title;
            changed_task.Description = task.Description;
            changed_task.Time = task.Time;
            changed_task.Category = task.Category;
            //Don't need to change CreatedAt
            changed_task.UpdatedAt = DateTime.Now;

            entities.SaveChanges();

            return Ok();
        }
    }
}
