﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TodoListEntityFramework;

namespace TodoListWebApi.Controllers {
    public class CategoriesController : ApiController {
        private TODOLISTEntities db = new TODOLISTEntities();

        // GET: api/Categories
        public List<Category> GetCategories() {
            return db.Categories.ToList();
        }

        // GET: api/Categories/5
        [ResponseType(typeof(Category))]
        public IHttpActionResult GetCategory(int id) {
            Category category = db.Categories.Find(id);
            if (category == null) {
                return NotFound();
            }

            return Ok(category);
        }

        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCategory(int id, Category category) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            if (id != category.CategoryID) {
                return BadRequest();
            }

            db.Entry(category).State = EntityState.Modified;

            try {
                db.SaveChanges();
            } catch (DbUpdateConcurrencyException) {
                if (!CategoryExists(id)) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Categories
        [ResponseType(typeof(Category))]
        public IHttpActionResult PostCategory(Category category) {
            ModelState.Remove("CreatedAt");
            ModelState.Remove("UpdatedAt");
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            var now = DateTime.Now;
            category.CreatedAt = now;
            category.UpdatedAt = now;

            db.Categories.Add(category);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = category.CategoryID }, category);
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(Category))]
        public IHttpActionResult DeleteCategory(int id) {
            Category category = db.Categories.Find(id);
            if (category == null) {
                return NotFound();
            }

            db.Categories.Remove(category);
            db.SaveChanges();

            return Ok(category);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id) {
            return db.Categories.Count(e => e.CategoryID == id) > 0;
        }
    }
}