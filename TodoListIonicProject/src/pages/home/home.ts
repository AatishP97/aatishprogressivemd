import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestProvider} from "../../providers/rest/rest";
import { ModalController } from "ionic-angular";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  users: any;
  tasks: any;
  categories: any;

  constructor(public navCtrl: NavController, public restProvider: RestProvider, public modalCtrl: ModalController) {
    console.log('Constructor hit');
    this.getUsers();
    this.getTasksForUser("Aatish");
  }

  addTaskModal() {
    var addTaskPage = this.modalCtrl.create('AddTaskPage');
    addTaskPage.present();
  }

  getUsers() {
    this.restProvider.getUsers().then(data => {
      this.users = data;
      console.log(this.users);
    })
  }

  getTasks() {
    this.restProvider.getTasks().then(data => {
      this.tasks = data;
      console.log(this.tasks);
    })
  }

  getTasksForUser(username: String) {
    this.restProvider.getTasksForUser(username).then(data => {
      this.tasks = data;
      console.log(this.tasks);
    })
  }

}
