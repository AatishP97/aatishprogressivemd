import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider} from "../../providers/rest/rest";

/**
 * Generated class for the AddTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})
export class AddTaskPage {

  task = {title: '', description: '', time: '', category: '3'}
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public restProvider: RestProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTaskPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addTask() {
    this.restProvider.postTask(this.task).then((result) => {
      console.log(result);
    }, (err) => {
      console.log(err);
    });
  }
}
