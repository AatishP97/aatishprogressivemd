import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  apiUrl = 'http://localhost:52596/api';

  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }

  //Fetch all users
  getUsers() {
    console.log('Getting users...');
    return new Promise (resolve => {
      this.http.get(this.apiUrl+'/users').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  //Fetch all categories
  getCategories() {
    console.log('Getting categories...');
    return new Promise (resolve => {
      this.http.get(this.apiUrl+'/categories').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  //Fetch all tasks
  getTasks() {
    console.log('Getting tasks...');
    return new Promise (resolve => {
      this.http.get(this.apiUrl+'/tasks').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  //Fetch tasks for a specific user
  getTasksForUser(username: String) {
    console.log('Getting tasks for user' + username);
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/tasks/?username=' + username).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  //POST a task
  postTask(task) {
    console.log('POSTing task...');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/tasks', JSON.stringify(task), {
        headers: new HttpHeaders().set('Content-Type', 'Application/json')
      })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}
